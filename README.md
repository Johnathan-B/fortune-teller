## Fortune Teller

---

# Description

This program automatically sends fortunes from the program `fortune` to a list of phone numbers at 08:00 every day.

---

# Building

This package has several dependencies, including the AWS C++ SDK. That can be built and installed using Amazon's instructions. The only required modules are the base module and the SNS module, which can be build individually if no other tools will be used to save both space and build time.

This package also depends on `fortune`.

To build this package, a simple `make` will work fine.


However, deb package building is included and relatively easy with the `dpkg-buildpackage` command. Recommended usage is to make a tarball of the source code using `tar --exclude-vcs -czvf ../fortune-teller_0.9.tar.gz ./` in the source directory. Then the command `dpkg-buildpackage -uc -us` can be used to automatically generate the deb package and the corresponding src package. Then the deb package can be installed as normal with `apt`, `aptitude`, or `dpkg`.

---

# Configuration

| Description | File Location |
| ----------------- | ----------------------------------- |
| Phone number list | `/etc/fortune-teller/phone-numbers` |
| Cron scheduling | `/etc/cron.d/fortune-teller` |
| Fortune input | `/usr/bin/send-fortune.sh` |

---
