# Compiler flags
CPP=g++
CPPFLAGS=-O2

LIBS = -laws-cpp-sdk-core -laws-cpp-sdk-sns

# Where to install the binaries
BIN = $(DESTDIR)/usr/bin

# Config files
CONFIG = $(DESTDIR)/etc/fortune-teller

# Build sections
all: fortune-teller

fortune-teller: fortune-teller.cpp
	$(CPP) -o fortune-teller fortune-teller.cpp $(CPPFLAGS) $(LIBS)

install: fortune-teller
	install -d $(BIN) $(DESTDIR)/etc
	install ./fortune-teller $(BIN)
	install -m644 send-fortune.sh $(BIN)
	touch $(DESTDIR)/etc/phone-numbers

clean:
	rm -f ./fortune-teller
