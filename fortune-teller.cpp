// Johnathan Burns
// 2018-2020
//

#include "fortune-teller.h"

static const std::string phone_number_file_name = "/etc/fortune-teller/phone-numbers";

// Sends an asynchronous AWS SNS request posting the for
void FortuneTeller::send_message(Aws::String fortune)
{
	int i;

	// Variables to read the phone numbers from the file defined above
	std::ifstream phone_number_file(phone_number_file_name);
	Aws::String phone_number;
	std::vector<Aws::String> phone_numbers;

	// Initialize the AWS SDK
	Aws::SDKOptions options;
	Aws::InitAPI(options);

	Aws::Client::ClientConfiguration client_config;
	Aws::SNS::SNSClient sns(client_config);
	Aws::SNS::Model::PublishRequest sns_req;

	// Print the fortune if we're in debug mode
	std::cout << "Your fortune is: " << std::endl << fortune << std::flush;
	// Set the message body to the fortune
	sns_req.SetMessage(fortune);

	// Read all the phone numbers from the phone number file
	if(!phone_number_file)
    {
        std::cout << "Error: Unable to access file: " << phone_number_file_name << std::endl;
        return;
    }
    else
    {
        while(phone_number_file >> phone_number) phone_numbers.push_back(phone_number);
	    phone_number_file.close();
    }

	for(i = 0; i < phone_numbers.size(); i++)
	{
        std::cout << "Attempting message to " << phone_numbers[i] << std::endl;

		sns_req.SetPhoneNumber(phone_numbers[i]);

		auto publish_result = sns.Publish(sns_req);

		if(publish_result.IsSuccess())
			std::cout << "Success: message sent to " << phone_numbers[i] << std::endl;
		else
			std::cout << "Failure: message not sent to " << phone_numbers[i] << std::endl;
	}

	Aws::ShutdownAPI(options);
}

// Reads the fortune from stdin and returns it in an AWS type string
Aws::String FortuneTeller::get_new_fortune()
{
	int i;
	Aws::String line;
	std::vector <Aws::String> input;

	while(std::getline(std::cin, line))
	{
		input.push_back(line);
	}

	line = "";

	for(i = 0; i < input.size(); i++)
	{
		line.append(input[i]);
		line.append("\n");
	}

	return line;
}

int main(int argc, char **argv)
{
	FortuneTeller *ft;

	ft = new FortuneTeller();

	ft->send_message(ft->get_new_fortune());

	exit(0);
}
