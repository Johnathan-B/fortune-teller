#include <vector>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <string>
#include <aws/core/Aws.h>
#include <aws/sns/SNSClient.h>
#include <aws/sns/model/PublishRequest.h>

class FortuneTeller
{
	    public:
			        void send_message(Aws::String fortune);
					Aws::String get_new_fortune();
};
